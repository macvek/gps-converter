const converter = require('../converter');

test('dms -> dm: Chile', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: 35,
      minutes: 40,
      seconds: 30.529
    },
    longitude:	{
      hemisphere: "W", 
      degrees: 71,
      minutes: 32,
      seconds: 34.685          
    }
  };
  
  const result = converter(location, "dm");
  
  expect(result).toEqual({
    type: "dm",
    latitude: { 
      hemisphere: "S", 
      degrees: "35",
      minutes: "40.509"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "71",
      minutes: "32.578"
    }          
  });
})

test('dms -> dm: Ljubljana', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: 46,
      minutes: 3,
      seconds: 25.006
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 14,
      minutes: 30,
      seconds: 20.704          
    }
  };
  
  const result = converter(location, "dm");
  
  expect(result).toEqual({
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: "46",
      minutes: "3.417"
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "14",
      minutes: "30.345"
    }         
  });
})

test('dms -> dm: New York', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: 40,
      minutes: 44,
      seconds: 30.822
    },
    longitude:	{
      hemisphere: "W", 
      degrees: 73,
      minutes: 59,
      seconds: 21.509          
    }
  };
  
  const result = converter(location, "dm");
  
  expect(result).toEqual({
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: "40",
      minutes: "44.514"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "73",
      minutes: "59.358"
    }
  });
})

test('dms -> dm: Auckland', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: 36,
      minutes: 50,
      seconds: 54.452
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 174,
      minutes: 45,
      seconds: 47.992          
    }
  };
  
  const result = converter(location, "dm");
  
  expect(result).toEqual({
    type: "dm",
    latitude: { 
      hemisphere: "S", 
      degrees: "36",
      minutes: "50.908",
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "174",
      minutes: "45.800",
    }
  });
})

//--------------------------------
//verification of Miljko's results

test('dms -> dm: Miljko #1 Ljubljana', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: 46,
      minutes: 3,
      seconds: 25.0
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 14,
      minutes: 30,
      seconds: 20.7          
    }
  };
  
  const result = converter(location, "dm");
  
  expect(result).toEqual({
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: "46",
      minutes: "3.417",
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "14",
      minutes: "30.345",
    }         
  });
})

test('dms -> dm: Miljko #2 Chile', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: 35,
      minutes: 40,
      seconds: 30.5
    },
    longitude:	{
      hemisphere: "W", 
      degrees: 71,
      minutes: 32,
      seconds: 34.7
    }
  };
  
  const result = converter(location, "dm");
  
  expect(result).toEqual({
    type: "dm",
    latitude: { 
      hemisphere: "S", 
      degrees: "35",
      minutes: "40.508"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "71",
      minutes: "32.578" 
    }          
  });
})