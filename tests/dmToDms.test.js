const converter = require('../converter');

test('dm -> dms: Chile', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "S",
      degrees: 35,
      minutes: 40.509
    },
    longitude:	{
      hemisphere: "W",
      degrees: 71,
      minutes: 32.578              
    }
  };

  const result = converter(location, "dms");

    expect(result).toEqual({
      type: "dms",
      latitude: { 
        hemisphere: "S", 
        degrees: "35",
        minutes: "40",
        seconds: "30.540"
      },
      longitude:	{
        hemisphere: "W", 
        degrees: "71",
        minutes: "32",
        seconds: "34.680"
      }
  });
})

test('dm -> dms: Ljubljana', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: 46,
      minutes: 3.417
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 14,
      minutes: 30.345
    }   
  };

  const result = converter(location, "dms");

    expect(result).toEqual({
      type: "dms",
      latitude: { 
        hemisphere: "N", 
        degrees: "46",
        minutes: "3",
        seconds: "25.020"
      },
      longitude:	{
        hemisphere: "E", 
        degrees: "14",
        minutes: "30",
        seconds: "20.700"
      }
  });
})

test('dm -> dms: New York', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: 40,
      minutes: 44.514
    },
    longitude:	{
      hemisphere: "W", 
      degrees: 73,
      minutes: 59.358
    }
  };

  const result = converter(location, "dms");

    expect(result).toEqual({
      type: "dms",
      latitude: { 
        hemisphere: "N", 
        degrees: "40",
        minutes: "44",
        seconds: "30.840"
      },
      longitude:	{
        hemisphere: "W", 
        degrees: "73",
        minutes: "59",
        seconds: "21.480"           
      }
  });
})

test('dm -> dms: Auckland', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "S", 
      degrees: 36,
      minutes: 50.908,
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 174,
      minutes: 45.800,
    }
  };

  const result = converter(location, "dms");

    expect(result).toEqual({
      type: "dms",
      latitude: { 
        hemisphere: "S", 
        degrees: "36",
        minutes: "50",
        seconds: "54.480"
      },
      longitude:	{
        hemisphere: "E", 
        degrees: "174",
        minutes: "45",
        seconds: "48.000"          
      }
  });
})