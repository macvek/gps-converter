const matchInputWithMask = require('../regex');

test('#1 valid input format', () => {
  const userInput = "12.1234, -45.6789";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual({
      type: "dd",
      latitude: "12.1234",
      longitude: "-45.6789"
    });
})

test('#2 invalid input format', () => {
  const userInput = "aaa, -45.bbb6789";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual("unsupported input format");
})

test('#3 valid input format (up to 7 decimals)', () => {
  const userInput = "-123.1234677, -145.6789567";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual({
      type: "dd",
      latitude: "-123.1234677",
      longitude: "-145.6789567"
    });
})

test('#4 invalid format (MORE then 7 decimals)', () => {
  const userInput = "-123.1234677234234, -145.678956745645645";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual("unsupported input format");
})

test('#5 invalid format (integer more then 180)', () => {
  const userInput = "-181.1234, -145.6789";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual("unsupported input format");
})

test('#6 invalid format (no space between values)', () => {
  const userInput = "-181.1234,-145.6789";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual("unsupported input format");
})

test('#7 invalid format (no comma between values)', () => {
  const userInput = "-181.1234 -145.6789";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual("unsupported input format");
})

test('#8 invalid format (no space and comma between values)', () => {
  const userInput = "-181.1234145.6789";

  const result = matchInputWithMask(userInput);

    expect(result).toEqual("unsupported input format");
})