const matchInputWithMask = require('../regex');

test('valid input', () => {
  const userInput = "N120 45 13.3, E99 60 8.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: "120",
      minutes: "45",
      seconds: "13.3"
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "99",
      minutes: "60",
      seconds: "8.9"      
    }
  });
})

test('valid input', () => {
  const userInput = "S15 60 44.0, W1 1 59.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "15",
      minutes: "60",
      seconds: "44.0"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "1",
      minutes: "1",
      seconds: "59.9"      
    }
  });
})

test('invalid input: degress > 180 = TEST FAILED', () => {
  const userInput = "S181 60 44.0, W1 1 59.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual("unsupported input format");
})

test('issue #1 - Test passed even thought that seconds shouldnt be more than 60', () => {
  const userInput = "S15 60 60.9, W1 1 60.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "15",
      minutes: "60",
      seconds: "60.9"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "1",
      minutes: "1",
      seconds: "60.9"      
    }
  });
})

test('invalid hemisphere E for latitude', () => {
  const userInput = "E180 60 44.0, W1 1 59.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual("unsupported input format");
})

test('invalid hemisphere W for latitude', () => {
  const userInput = "W180 60 44.0, E1 1 59.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual("unsupported input format");
})

test('invalid hemisphere S for longitude', () => {
  const userInput = "N180 60 44.0, S1 1 59.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual("unsupported input format");
})

test('invalid hemisphere N for longitude', () => {
  const userInput = "S180 60 44.0, N1 1 59.9";
  
  const result = matchInputWithMask(userInput);
  
  expect(result).toEqual("unsupported input format");
})