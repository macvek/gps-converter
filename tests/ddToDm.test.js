const converter = require('../converter');

//Input number has 6 decimals (google maps format)

test('dd -> dm: Chile', () => {
  const location = {
    type: "dd",
    latitude: -35.675147,
    longitude: -71.542968
  };

  const result = converter(location, "dm");

    expect(result).toEqual({
      type: "dm",
      latitude: { 
        hemisphere: "S", 
        degrees: "35",
        minutes: "40.509"
      },
      longitude:	{
        hemisphere: "W", 
        degrees: "71",
        minutes: "32.578"
      }  
  });
})

test('dd -> dm: Ljubljana', () => {
  const location = {
    type: "dd",
    latitude: 46.056946,
    longitude: 14.505751
  };

  const result = converter(location, "dm");

    expect(result).toEqual({
      type: "dm",
      latitude: { 
        hemisphere: "N", 
        degrees: "46",
        minutes: "3.417"
      },
      longitude:	{
        hemisphere: "E", 
        degrees: "14",
        minutes: "30.345"
      }        
  });
})

test('dd -> dm: New York', () => {
  const location = {
    type: "dd",
    latitude: 40.741895,
    longitude: -73.989308
  };

  const result = converter(location, "dm");

    expect(result).toEqual({
      type: "dm",
      latitude: { 
        hemisphere: "N", 
        degrees: "40",
        minutes: "44.514"
      },
      longitude:	{
        hemisphere: "W", 
        degrees: "73",
        minutes: "59.358"
      }
  });
})

test('dd -> dm: Auckland', () => {
  const location = {
    type: "dd",
    latitude: -36.848459,
    longitude: 174.763331
  };

  const result = converter(location, "dm");

    expect(result).toEqual({
      type: "dm",
      latitude: { 
        hemisphere: "S", 
        degrees: "36",
        minutes: "50.908",
      },
      longitude:	{
        hemisphere: "E", 
        degrees: "174",
        minutes: "45.800",
      }
  });
})
