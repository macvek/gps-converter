const converter = require('../converter');

//Input number has 6 decimals (google maps format)

test('dd -> dms: Chile', () => {
  const location = {
    type: "dd",
    latitude: -35.675147,
    longitude: -71.542968
  };
  
  const result = converter(location, "dms");
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "35",
      minutes: "40",
      seconds: "30.529"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "71",
      minutes: "32",
      seconds: "34.685"             
    }
  });
})

test('dd -> dms: Ljubljana', () => {
  const location = {
    type: "dd",
    latitude: 46.056946,
    longitude: 14.505751
  };
  
  const result = converter(location, "dms");
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: "46",
      minutes: "3",
      seconds: "25.006"
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "14",
      minutes: "30",
      seconds: "20.704"             
    }
  });
})

test('dd -> dms: New York', () => {
  const location = {
    type: "dd",
    latitude: 40.741895,
    longitude: -73.989308
  };
  
  const result = converter(location, "dms");
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: "40",
      minutes: "44",
      seconds: "30.822"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "73",
      minutes: "59",
      seconds: "21.509"             
    }
  });
})

test('dd -> dms: Auckland', () => {
  const location = {
    type: "dd",
    latitude: -36.848459,
    longitude: 174.763331
  };
  
  const result = converter(location, "dms");
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "36",
      minutes: "50",
      seconds: "54.452"
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "174",
      minutes: "45",
      seconds: "47.992"             
    }
  });
})

//BigNumber Tests
test('dd -> dms: Chile BigNumber', () => {
  const location = {
    type: "dd",
    latitude: -35.675147,
    longitude: -71.5429689999999
  };
  
  const result = converter(location, "dms");
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "35",
      minutes: "40",
      seconds: "30.529"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "71",
      minutes: "32",
      seconds: "34.688"             
    }
  });
})

test('dd -> dms: RANDOM PLACE BigNumber', () => {
  const location = {
    type: "dd",
    latitude: -36.2708497744328,
    longitude: -71.5704348203124
  };
  
  const result = converter(location, "dms");
  
  expect(result).toEqual({
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "36",
      minutes: "16",
      seconds: "15.059"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "71",
      minutes: "34",
      seconds: "13.565"             
    }
  });
})