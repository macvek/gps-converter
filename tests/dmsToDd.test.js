const converter = require('../converter');

//returned value has 6 decimals (google maps format)

test('dms -> dd -> dms: Chile', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "35",
      minutes: "40",
      seconds: "30.529"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "71",
      minutes: "32",
      seconds: "34.685"      
    }
  };

  const ddResult = converter(location, "dd");
  const dmsResult = converter(ddResult, "dms");
  
  expect(dmsResult).toEqual(location);
})

test('dms -> dd -> dms: Ljubljana', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: "46",
      minutes: "3",
      seconds: "25.006"
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "14",
      minutes: "30",
      seconds: "20.704"          
    }
  };
  
  const ddResult = converter(location, "dd");
  const dmsResult = converter(ddResult, "dms");
  
  expect(dmsResult).toEqual(location);
})

test('dms -> dd -> dms: New York', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: "40",
      minutes: "44",
      seconds: "30.822"
    },
    longitude:	{
      hemisphere: "W", 
      degrees: "73",
      minutes: "59",
      seconds: "21.509"          
    }
  };
  
  const ddResult = converter(location, "dd");
  const dmsResult = converter(ddResult, "dms");
  
  expect(dmsResult).toEqual(location);
})

test('dms -> dd -> dms: Auckland', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "36",
      minutes: "50",
      seconds: "54.452"
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "174",
      minutes: "45",
      seconds: "47.992"          
    }
  };
  
  const ddResult = converter(location, "dd");
  const dmsResult = converter(ddResult, "dms");
  
  expect(dmsResult).toEqual(location);
})

test('dms -> dd: Chile', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: 35,
      minutes: 40,
      seconds: 30.529
    },
    longitude:	{
      hemisphere: "W", 
      degrees: 71,
      minutes: 32,
      seconds: 34.685
    }
  };
  
  const result = converter(location, "dd");
  
  expect(result).toEqual({
    type: "dd",
    latitude: "-35.675147",
    longitude: "-71.542968"
  });
})

//--------------------------------
//verification of Miljko's results

test('dms -> dd: Miljko #1 Ljubljana', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "N", 
      degrees: "46",
      minutes: "3",
      seconds: "25.0" // 25.007
    },
    longitude:	{
      hemisphere: "E", 
      degrees: "14",
      minutes: "30",
      seconds: "20.7" // 20.705         
    }
  };
  
  const result = converter(location, "dd");
  
  expect(result).toEqual({
    type: "dd",
    latitude: "46.056944", // 46.05695 
    longitude: "14.505750" // 14.50575  
  });
})

test('dms -> dd: Miljko #2 Chile', () => {
  const location = {
    type: "dms",
    latitude: { 
      hemisphere: "S", 
      degrees: "35",
      minutes: "40",
      seconds: "30.5" // 30.529
    },
    longitude:	{
      hemisphere: "W",
      degrees: "71",
      minutes: "32",
      seconds: "34.7" // 34.688         
    }
  };
  
  const result = converter(location, "dd");
  
  expect(result).toEqual({
    type: "dd",
    latitude: "-35.675139", // -35.67515
    longitude: "-71.542972" // -71.54297 
  });
})