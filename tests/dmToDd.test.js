const converter = require('../converter');

test('dm -> dms: Chile', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "S",
      degrees: 35,
      minutes: 40.509
    },
    longitude:	{
      hemisphere: "W",
      degrees: 71,
      minutes: 32.578              
    }
  };

  const result = converter(location, "dd");

    expect(result).toEqual({
      type: "dd",
      latitude: "-35.675150", 
      longitude: "-71.542967" 
  });
})

test('dm -> dms: Ljubljana', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: 46,
      minutes: 3.417
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 14,
      minutes: 30.345
    }   
  };

  const result = converter(location, "dd");

    expect(result).toEqual({
      type: "dd",
      latitude: "46.056950", 
      longitude: "14.505750" 
  });
})

test('dm -> dms: New York', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "N", 
      degrees: 40,
      minutes: 44.514
    },
    longitude:	{
      hemisphere: "W", 
      degrees: 73,
      minutes: 59.358
    }
  };

  const result = converter(location, "dd");

    expect(result).toEqual({
      type: "dd",
      latitude: "40.741900", 
      longitude: "-73.989300" 
  });
})

test('dm -> dms: Auckland', () => {
  const location = {
    type: "dm",
    latitude: { 
      hemisphere: "S", 
      degrees: 36,
      minutes: 50.908,
    },
    longitude:	{
      hemisphere: "E", 
      degrees: 174,
      minutes: 45.800,
    }
  };

  const result = converter(location, "dd");

    expect(result).toEqual({
      type: "dd",
      latitude: "-36.848467", 
      longitude: "174.763333" 
  });
})
