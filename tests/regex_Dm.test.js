const matchInputWithMask = require('../regex');

test('valid input', () => {
  const userInput = "N120 60.000, E96 16.379";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual({
    type: "dm",
    latitude: {
      hemisphere: "N",
      degrees: "120",
      minutes: "60.000",
    },
    longitude: {
      hemisphere: "E",
      degrees: "96",
      minutes: "16.379",
    }
  });
})

test('valid input', () => {
  const userInput = "S25 45.123, W133 45.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual({
    type: "dm",
    latitude: {
      hemisphere: "S",
      degrees: "25",
      minutes: "45.123",
    },
    longitude: {
      hemisphere: "W",
      degrees: "133",
      minutes: "45.987",
    }
  });
})

test('invalid input: degress > 180 = TEST FAILED', () => {
  const userInput = "S181 45.123, W133 45.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual("unsupported input format");
})

test('issue #1 - Test passed even thought that minutes shouldnt be more than 60', () => {
  const userInput = "S25 60.123, W133 60.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual({
    type: "dm",
    latitude: {
      hemisphere: "S",
      degrees: "25",
      minutes: "60.123",
    },
    longitude: {
      hemisphere: "W",
      degrees: "133",
      minutes: "60.987",
    }
  });
})

test('invalid hemisphere E for latitude', () => {
  const userInput = "E180 45.123, W133 45.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual("unsupported input format");
})

test('invalid hemisphere W for latitude', () => {
  const userInput = "W180 45.123, E133 45.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual("unsupported input format");
})

test('invalid hemisphere S for longitude', () => {
  const userInput = "N180 45.123, S133 45.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual("unsupported input format");
})

test('invalid hemisphere N for longitude', () => {
  const userInput = "S180 45.123, N133 45.987";

  const result = matchInputWithMask(userInput);

  expect(result).toEqual("unsupported input format");
})