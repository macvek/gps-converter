const BigNumber = require('bignumber.js');
const ONE_SECOND_IN_MINUTES = BigNumber(0.01666667);

const calculateFromMinutesOnlyToMinutesAndSeconds = ({hemisphere, degrees, minutes, seconds}) => {
    const minutesInteger = BigNumber(parseInt(minutes));
  
    return {
      hemisphere,
      degrees: degrees.toString(),
      minutes: minutesInteger.toString(),
      seconds: BigNumber(minutes)
        .minus(minutesInteger)
        .dividedBy(ONE_SECOND_IN_MINUTES)
        .toFixed(3)
    };
  };
  
  module.exports = function dmToDms({latitude, longitude}) {
    return {
      type: "dms",
      latitude: calculateFromMinutesOnlyToMinutesAndSeconds(latitude),
      longitude: calculateFromMinutesOnlyToMinutesAndSeconds(longitude)
    };
  };