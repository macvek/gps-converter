const dmsToDm = require('../converters/dmsToDm');
const ddToDms = require('../converters/ddToDms');

module.exports = function ddToDm(location) {
    return dmsToDm(ddToDms(location));
  };