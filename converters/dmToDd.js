const dmsToDd = require('../converters/dmsToDd');
const dmToDms = require('../converters/dmToDms');

module.exports = function dmToDd(location) {
  return dmsToDd(dmToDms(location));
};