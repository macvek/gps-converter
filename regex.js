const ddRegEx = /^(-?[0-9]|-?[1-9][0-9]|-?1[1-7][0-9]|-?180)\b(\.)?([0-9]{0,7})\, (-?[0-9]|-?[1-9][0-9]|-?1[1-7][0-9]|-?180)\b(\.)?([0-9]{0,7})/;
//input must be in format: "12.1234, -45.6789"

const dmsRegEx = /^([N]|[S])([0-9]|[1-9][0-9]|1[1-7][0-9]|180)\b(\ )([0-9]|[1-5][0-9]|60)\b(\ )([0-9]|[1-5][0-9]|60)(\.)([0-9])\b, ([E]|[W])([0-9]|[1-9][0-9]|1[1-7][0-9]|180)\b(\ )([0-9]|[1-5][0-9]|60)\b(\ )([0-9]|[1-5][0-9]|60)(\.)([0-9])\b/;
//input must be in format: "N120 45 13.3, S99 60 8.9"
//degress must be < 180
//minutes & seconds must be < 60
//seconds must have exactly 1x decimal

const dmRegEx = /^([N]|[S])([0-9]|[1-9][0-9]|1[1-7][0-9]|180)\b(\ )([0-9]|[1-5][0-9]|60)(\.)([0-9][0-9][0-9])\b, ([E]|[W])([0-9]|[1-9][0-9]|1[1-7][0-9]|180)\b(\ )([0-9]|[1-5][0-9]|60)(\.)([0-9][0-9][0-9])\b/;
//input must be in format: "N120 60.000, S96 16.379"
//degress must be < 180
//minutes must be < 60
//minutes must have exactly 3x decimals

module.exports = function matchInputWithMask(userInput) {
  const matchDD = ddRegEx.exec(userInput);
  const matchDMS = dmsRegEx.exec(userInput);
  const matchDM = dmRegEx.exec(userInput);

  if (matchDD) {
    return {
      type: "dd",
      latitude: `${matchDD[1]}.${matchDD[3]}`,
      longitude: `${matchDD[4]}.${matchDD[6]}`
    };

  } if (matchDMS) {
    return {
      type: "dms",
      latitude: {
        hemisphere: matchDMS[1],
        degrees: matchDMS[2],
        minutes: matchDMS[4],
        seconds: `${matchDMS[6]}.${matchDMS[8]}`
      },
      longitude: {
        hemisphere: matchDMS[9],
        degrees: matchDMS[10],
        minutes: matchDMS[12],
        seconds: `${matchDMS[14]}.${matchDMS[16]}`
      }
    };

  } if (matchDM) {
    return {
      type: "dm",
      latitude: {
        hemisphere: matchDM[1],
        degrees: matchDM[2],
        minutes: `${matchDM[4]}.${matchDM[6]}`
      },
      longitude: {
        hemisphere: matchDM[7],
        degrees: matchDM[8],
        minutes: `${matchDM[10]}.${matchDM[12]}`
      }
    }
  }

  return "unsupported input format";
};