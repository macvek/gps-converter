const dmsToDm = require('./converters/dmsToDm');
const dmsToDd = require('./converters/dmsToDd');
const ddToDms = require('./converters/ddToDms');
const ddToDm = require('./converters/ddToDm');
const dmToDms = require('./converters/dmToDms');
const dmToDd = require('./converters/dmToDd');

module.exports = function converter(location, convertTo) {
  const logicToSelectConversion = {
    dms: {
      dm: dmsToDm,
      dd: dmsToDd
      },
    
    dd: {
      dms: ddToDms,
      dm: ddToDm
    },
  
    dm: {
      dms: dmToDms,
      dd: dmToDd,
    }
  };

  return logicToSelectConversion[location.type][convertTo](location);
};